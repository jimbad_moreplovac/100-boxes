/*	IMPORTANT COMMENTS */
/*	Due to inability to solve linebrak-style problem without changing the eslintrc file */
/*	I was forced to let eslint to fix the error by disabling the rule */
/*	eslint linebreak-style: ["error", "windows"] */
/*	Also I didn't manage to install fabric.js using npm because in order to install Fabric.js */
/*	it is required to install Cairo graphics manually which is a very complex proccess for windows users */
/*	so i just included fabric.min.js file in the index.html file and in order to remove the errors */
/*	caused by the rule no-undef (fabric is not defined), without changing the eslintrc file I disabled that rule */

/* function for returning object by id */
// eslint-disable-next-line no-undef
fabric.Canvas.prototype.getItemByName = function objname(id) {
	let object = null;
	const objects = this.getObjects();
	for (let i = 0, len = this.size(); i < len; i += 1) {
		if (objects[i].id && objects[i].id === id) {
			object = objects[i];
			break;
		}
	}

	return object;
};

/* variables */

let opacity = '';
let isclicked = false;
let hoverCursor = '';
let topp = 0;
let leftt = 0;
let fill = 'white';
let arrayForDelete = [];
const columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
// eslint-disable-next-line no-undef
const canvas = new fabric.Canvas('Canvas', { width: 501, height: 501 });
let clickedArray = [];

/* function for creating the playing board, setting the properties for rectangle objects and adding them on the canvas */
/* custom properties are added for example an id property is made by combining the letter from columns array (x axis) and ordinal number of object (y axis) */
function initboard() {
	columns.forEach((index, count) => {
		topp = count * 50;
		for (let j = 0; j < 10; j += 1) {
			leftt = j * 50;
			const idforobject = index + (j + 1);
			// eslint-disable-next-line no-undef
			const rect = new fabric.Rect({
				id: idforobject,
				left: leftt,
				top: topp,
				fill,
				strokeWidth: 1,
				stroke: 'rgb(238,238,238)',
				width: 50,
				height: 50,
				x: index,
				xnum: columns.indexOf(index),
				y: j + 1,
				isclicked,
				text: idforobject,
				selectable: false,
				hasBorders: false,
				hoverCursor: 'arrow',
			});
			canvas.add(rect);
		}
	});
}
/*	function for creating a pattern of potential clickable objects around the clicked object */
/*	function parameters */
/*	arr (array of potential clickable object ids') */
/*	selected (mark of array, to determine if the array holds clicked objects or potential clickable objects)  */
/*	the result is colouring the clicked objects red and disabling them for further clicking */
function createpattern(arr, selected) {
	if (selected === 'T') {
		opacity = '0.8';
		isclicked = true;
		hoverCursor = 'arrow';
	} else {
		opacity = '0.4';
		isclicked = false;
		hoverCursor = 'pointer';
	}
	for (let i = 0; i < arr.length; i += 1) {
		if (canvas.getItemByName(arr[i]) !== null) {
			canvas.getItemByName(arr[i]).set({
				fill: 'red',
				hoverCursor,
				opacity,
				isclicked,
			});
		}
	}
}
/* function for errasing the pattern of potential clickable objects, leaving the clicked objects coloured */
function erasepattern(arr) {
	for (let i = 0; i < arr.length; i += 1) {
		if (canvas.getItemByName(arr[i]) !== null) {
			canvas.getItemByName(arr[i]).set({
				fill: 'white',
				hoverCursor: 'arrow',
			});
		}
	}
}
initboard();

/* 	function that creates a "mouse:down" event listener onto canvas */
/* 	the conditions explained */
/* 	it is not allowed for object to be clicked more than once (object.target.clicked) */
/*	in the first move it is allowed to click on any object */
/*	in the following moves it can only be clicked on object in arrayForDelete (array of optional clickable objects) */
/*	array of optional clickable objects is emptied if not empty */
/*	from the selected object's properties, an array is made of optional clickable objects */
/*	array is filtered only to contain clickable objects on the field (which is later to be erased from the canvas) */
/*	selected object is pushed in the clickedArrey array (which is not later to be erased from the canvas) */
/* 	function for creating patterns is called with each of the arrays and their marks as parameters */
/*	when the array of potential clickable objects is empty the game ends */
canvas.on('mouse:down', (object) => {
	let array = [];
	if ((object.target.clicked !== true && (clickedArray.length === 0)) || (object.target.clicked !== true && arrayForDelete.includes(object.target.id))) {
		if (arrayForDelete.length !== 0) {
			erasepattern(arrayForDelete);
		}
		clickedArray.push(object.target.id);

		const distanceRow = 3;
		const distanceColumns = 3;
		const distanceDiagonalRow = 2;
		const distanceDiagonalColumn = 1;

		const activeX = object.target.x;
		const activeNumX = object.target.xnum;
		const activeY = object.target.y;
		const previousRow = (activeNumX - distanceRow);
		const nextRow = (activeNumX + distanceRow);
		const previousColumn = activeY - distanceColumns;
		const nextColumn = activeY + distanceColumns;
		const up = columns[previousRow] + activeY;
		const upRight = columns[activeNumX - distanceDiagonalRow] + (nextColumn - distanceDiagonalColumn);
		const right = activeX + nextColumn;
		const downRigth = columns[activeNumX + distanceDiagonalRow] + (nextColumn - distanceDiagonalColumn);
		const down = columns[nextRow] + activeY;
		const downLeft = columns[activeNumX + distanceDiagonalRow] + (previousColumn + distanceDiagonalColumn);
		const left = activeX + previousColumn;
		const upLeft = columns[activeNumX - distanceDiagonalRow] + (previousColumn + distanceDiagonalColumn);
		array.push(up, upRight, right, downRigth, down, downLeft, left, upLeft);
		array = array.filter((v) => (v && (parseInt(v.substring(1), 10) <= 10) && (parseInt(v.substring(1), 10) > 0) && columns.includes(v[0])) && !clickedArray.includes(v) && !!v);
		arrayForDelete = array.slice();
		if (array.length === 0) {
			const endwrap = document.createElement('div');
			const end = document.createElement('div');
			end.innerHTML = 'Game over!';
			end.className = 'endtext';
			end.style.cssText = 'display: inline-block;color: white;font-size: 22px; transform: translate(0, -50%);margin-top: 50%;';
			endwrap.className = 'endtextwrap';
			endwrap.style.cssText = 'position: absolute;top: 100px;width: 200px;left: 150px;background-color: grey;height: 200px;text-align: center;';
			document.body.appendChild(endwrap);
			document.getElementsByClassName('endtextwrap')[0].appendChild(end);
			canvas.selection = false;
		}
		createpattern(array, 'F');
		createpattern(clickedArray, 'T');

		if (document.getElementsByClassName('reloadbutton').length < 1) {
			const button = document.createElement('button');
			button.innerHTML = 'Reset game';
			button.className = 'reloadbutton';
			button.onclick = function resetGame() {
				erasepattern(clickedArray);
				erasepattern(arrayForDelete);
				clickedArray = [];
				arrayForDelete = [];
				array = [];
				fill = 'white';
				isclicked = false;
				canvas.clear();
				if (document.getElementsByClassName('endtextwrap').length > 0) {
					document.getElementsByClassName('endtextwrap')[0].remove();
				}

				initboard();
			};
			button.style.cssText = 'color: white;outline: none;background: grey;border-radius: 3px;padding: 20px;font-size: 14px;border: 1px solid white; cursor: pointer';
			document.body.appendChild(button);
		}
	}
});
